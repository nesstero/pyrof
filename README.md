[![PyPI version](https://badge.fury.io/py/pyrof.svg)](https://badge.fury.io/py/pyrof)
# Pyrof
fork from [dynmen](https://github.com/frostidaho/dynmen)
Python wrappers for dynamic menus (rofi, fzf)

# Catatan
- [Catatan Pyrof](https://nesstero.gitlab.io/catatan/pyrof/)

# Install
```
pip install pyrof
```

## AUR
- [python-pyrof-git](https://aur.archlinux.org/packages/python-pyrof-git)

# Some examples of scripts that use pyrof
- [Al-Quran Rofi](https://gitlab.com/nesstero/Al-Quran-Rofi)
- [Ntranslate](https://gitlab.com/nesstero/ntranslate)
- [M-Monkey](https://gitlab.com/nesstero/m-monkey)
