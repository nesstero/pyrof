#!/usr/bin/env python
from setuptools import find_packages, setup

with open("README.md", "r") as des:
    l_desc = des.read()

setup(
    name='pyrof',
    packages=find_packages(),
    version='0.1.1',
    description='Python wrappers for dynamic menus (rofi, fzf)',
    url='https://gitlab.com/nesstero/pyrof',
    license='MIT',
    maintainer='nestero',
    maintainer_email='nestero@mail.com',
    install_requires=['traitlets'],
    long_description=l_desc,
    long_description_content_type='text/markdown',
)
